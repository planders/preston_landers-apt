package com.appspot.cee_me.android;


public class Config {
    public static final String PREFS_NAME = "Cee.me";
    public static final String PREF_AUTH_TOKEN = "authToken";
    public static final String PREF_ACCOUNT_NAME = "accountName";

    public static final String AUDIENCE = "server:client_id:860742061992.apps.googleusercontent.com";
    public static final boolean DEVELOPER_MODE = true;

    // how long will we wait for a location fix?
    public static final int MAX_LOCATION_WAIT_SEC = 10;

    // Stuff for intents, could move somewhere else
    public static final String NAV_OFFSET = "com.appspot.cee_me.android.NAV_OFFSET";
    public static final String NAV_LIMIT = "com.appspot.cee_me.android.NAV_LIMIT";
    public static final String LOCATION_SEARCH = "com.appspot.cee_me.android.LOCATION_SEARCH";
    public static final String SHOW_MY_SUBS = "com.appspot.cee_me.android.SHOW_MY_SUBS";
    public static final String SEARCH_TERM = "com.appspot.cee_me.android.SEARCH_TERM";
    public static final String STREAM_NAME = "com.appspot.cee_me.android.STREAM_NAME";
    public static final String STREAM_ID = "com.appspot.cee_me.android.STREAM_ID";
    public static final String STREAM_OWNER_ID = "com.appspot.cee_me.android.STREAM_OWNER_ID";
    public static final String MY_ID = "com.appspot.cee_me.android.MY_ID";
    public static final String IMAGES = "com.appspot.cee_me.android.IMAGES";
    public static final String IMAGE_LABELS = "com.appspot.cee_me.android.IMAGE_LABELS";
    public static final String IMAGE_POSITION = "com.appspot.cee_me.android.IMAGE_POSITION";
    public static final String STREAM_UPLOAD_URL = "com.appspot.cee_me.android.STREAM_UPLOAD_URL";
}
