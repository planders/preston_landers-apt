Advanced Programming Tools (APT) 2013 class 
===========================================

This is my git repository for Dr. Adnan Aziz's 2013 Advanced Programming Tools (APT) class in the Software Engineering MS program at the University of Texas at Austin.

Notable Projects
================

CeeMe: an Android app to share links, photos, videos, and other files with your friends. Uses Google App Engine for the server component and Amazon S3 storage.

Connexus: a Java web app and companion Android app for social photo sharing. Also uses GAE.
